package.path = 'extern/?.lua;' .. package.path
local SLAXML = require 'slaxdom'

local read_xml = {

	 root = nil,
	 doc  = nil,


}
local lvl = 0

function tab(num)
	return string.rep("\t",num)
end

function trim(s)
	P = string.gsub(s, "%s", "")
	return P
end


function print_node(node)
	lvl = lvl + 1
	if (type(node) == "table") then
		for k,v in pairs(node) do
			print(tab(lvl), k)
			if k~="parent" then
				print_node(v)
			end
		end
	elseif (type(node) == "number" or type(node) == "string") then
		print(tab(lvl),node)
	end
	lvl = lvl - 1
end


local tmp_lvl = 0
function get_value2(node,tbl,max_lvl)

	if tmp_lvl == max_lvl then
		tmp_lvl = tmp_lvl - 1
		return
	end



	if (type(node) == "table") then
		for k,v in pairs(node) do
			if k~="parent"  then

				--print(tab(tmp_lvl),k,v,tmp_lvl)
				tmp_lvl = tmp_lvl + 1

				if v.attr ~= nil and v.attr.id then

					if tbl[v.name] == nil then
						tbl[v.name] = {}
					end

					tbl[v.name][v.attr.id] = {}
					get_value2(v,tbl[v.name][v.attr.id],max_lvl)

				elseif v.kids ~= nil and v.kids[1].type =="text" and trim(v.kids[1].value) ~="" then

					tmp_lvl = tmp_lvl - 1
					tbl[v.name] = v.kids[1].value

				elseif v.type == "element" then

					tbl[v.name] = {}
					get_value2(v,tbl[v.name],max_lvl)

				else

					get_value2(v,tbl,max_lvl)

				end

			end
		end
	end

	tmp_lvl = tmp_lvl - 1
end



function  read_xml.read(path)
	xml  = io.open(path):read('*all')
	io.close()
	xml_doc = SLAXML:dom(xml)
	doc = {}
	get_value2(xml_doc.root,doc,20)

	return doc
end

function read_xml.print(node)
	print_node(node)
end



return read_xml
