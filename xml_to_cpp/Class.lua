
local setmetatable=setmetatable

local xml_reader = require "read_xml"

-- Hard coded stuff
local ret_val_str 	= 'ret_val'
local arg_str     	= 'argument'
local function_str 	= 'function'
local base_str		= 'base'
local include_str	= 'include'


-- MISC
function newline(num)
	return string.rep("\n",num)
end

function tab(num)
	return string.rep("\t",num)
end

function del_last(str)
	return string.gsub(str, "[^\128-\191][\128-\191]*$", "")
end

function space(num)
	if num == nil then
		num = 1
	end
	return string.rep(" ",num)
end

function bracket()
	return "{" .. newline(2) .. "}" .. newline(1)
end

function format_class_name(name)
	tmp = ""
	for token in string.gmatch(name, "[^%s]+") do
	   tmp = tmp .. (token:gsub("^%l", string.upper))
	end
	return "AR" .. tmp
end

function get_includes()
	return "#include \"" .. get_file_name("h") .. "\"" .. newline(2)
end

function get_file_name(class_name,ext)
	class_lower = string.lower(class_name)
	file_name = "ar"

	for token in string.gmatch(class_lower, "[^%s]+") do
		if file_name == "" then
			file_name = token
		else
			file_name = file_name .. "_" .. token
		end
	end
	return file_name .. "." .. ext

end

-- IO

function create_file(content,file_name)
	file = io.open(file_name,"w+")
	io.output(file)
	io.write(content)
	io.close()

end


-- Define guards
function get_def_prefix(upper_name)
	tmp = ""
	for token in string.gmatch(upper_name, "[^%s]+") do
	   tmp = tmp .. "_" .. token
	end
	return "_AR"..tmp .."_H"
end

function get_ifdef(upper_name)
	return "#ifndef " .. get_def_prefix(upper_name) .. newline(1)
end

function get_def(upper_name)
	return "#define " .. get_def_prefix(upper_name) .. newline(2)
end

function get_def_guard_start(class_name)
	class_upper = string.upper(class_name)
	return  get_ifdef(class_upper) .. get_def(class_upper)
end

function get_def_guard_end()
	return "#endif"
end



-- Function
local function Function(node,name)

	local self = {

	}
	local org_name = name
	local org_node = node

	function self.disp(content)
		if(type(node[content]) == 'table') then
			for k,v in pairs(node[content]) do
				print(k)
			end
		else
			print(node[content])
		end
	end

	-- Create return value string
	function self.retval()
		str = ""
		if(type(node[ret_val_str]) == 'table') then
			for k,v in pairs(node[content]) do
				str = str .. k
			end
		else
			str = node[ret_val_str] .. " "

		end

		if str == " " then
			str = ""
		end

		return str
	end

	-- Function names
	function self.def_name(class_name)
		return (class_name.."::" .. org_name )
	end


	function self.args()
		str = "("
		for k,v in pairs(node[arg_str]) do
			if v.arg_type ~= "void" then
				str = str .. v.arg_type .. " " .. k .. ", "
			end
		end

		if str ~= "(" then
			str = string.gsub(str, "[^\128-\191][\128-\191]*$", "")
			str = string.gsub(str, "[^\128-\191][\128-\191]*$", "")
		end

		return str .. ")"
	end




	function self.print_arg()
		disp(arg_str)
	end

	function self.print_retval()
		disp(ret_val_str)
	end

	function self.get_declaration(class_name)
		str = self.retval() .. org_name .. self.args() .. ";"
		return str
	end


	function self.get_definition(class_name)
		str = self.retval() .. self.def_name(class_name) .. self.args() .. bracket()
		return str
	end



	return self
end





-- Class
local Class  = {}
Class.__index = Class




setmetatable(Class, {
	__call = function (cls, ...)
		return cls.new(...)
	end,
})


function Class.new(path,name)

	xml = xml_reader.read(path)
	local self = setmetatable({}, Class)
	self.value  = xml
	self.name   = name
	self.format_name = format_class_name(name)
	self.functions ={}

	for k,v in pairs(self.value[function_str]) do

		if k == 'CONSTRUCTOR' then
			v.ret_val = ""
			self.functions[self.format_name] = Function(v,self.format_name)
		else
			self.functions[k] = Function(v,k)
		end


	end


	function self.def_guard_start()
		return get_def_guard_start(name)
	end

	function self.def_guard_start()
		return get_def_guard_start(name)
	end

	function base_classes()
		str = ""
		for k,v in pairs(self.value[base_str].class) do
			str = str .. v.type .. space() .. k .. "," ..  space()
		end
		str = del_last(str)
		str = del_last(str)
		str = str ..  newline(1) .. "{"
		return str
	end


	function includes()
		str = ""
		for k,v in pairs(self.value[include_str].file) do
			str = str .. "#include" .. space() .. "\"" .. k .. "\"" ..  newline(1)
		end
		return str
	end

	function append_accessability(str,acc)
		return str .. acc .. ":\n\n"
	end

	function self.create_header()

		res = get_def_guard_start(self.name) .. includes() .. newline(2) .. "class " .. self.format_name .. ": " .. base_classes() ..  newline(2)

		res = append_accessability(res,"public")
		for k,v in pairs(self.functions) do
			res = res .. tab(1) .. v.get_declaration(self.format_name) .. newline(2)
		end

		res = append_accessability(res,"protected")
		res = append_accessability(res,"private")

		res = res .. "};\n" .. get_def_guard_end()
		return res
	end

	function self.create_source()

		res = "#include " .. get_file_name(self.name,"h") .. newline(2)


		for k,v in pairs(self.functions) do
			res = res .. v.get_definition(self.format_name) .. newline(2)
		end


		return res
	end


	function self.gen_header_file()
		create_file(get_header_content(),get_file_name("h"))
	end

	function self.gen_cpp_file()
		create_file(gen_cpp_conent(),get_file_name("cpp"))
	end

	function self.gen_files()


		dir_list = io.popen("for /f %a in ('dir /b /s') do @echo %~na"):read("*all") --
		fields = {}
		io.close()
		sep = "\n"

		dir_list:gsub("([^"..sep.."]*)"..sep, function(c) table.insert(fields, c) end)

		found = false

		for k,v in pairs(fields) do
			if(v == "out") then
				found = true
			end
		end
		if(found == false) then
			io.popen("md out")
			print("creating directory 'out'")
			io.close()
		end


		create_file(self.create_source(),"out/" .. get_file_name(self.name,"cpp"))
		create_file(self.create_header(),"out/" .. get_file_name(self.name,"h"))
		print(get_file_name(self.name,"h") .. " and " .. get_file_name(self.name,"cpp") .. " created.")
	end


	return self
end







--xml = xml_reader.read("source_template.xml")
--xml_reader.print(xml)
--local instance = Class("source_template.xml","Test Class")
--print(instance.create_source())
--instance:print("function")
--funcs = instance:get_functions('function')


return Class


