local header_file = {
 class = "",
 is_grid_instance = false
}

-- Misc

function newline(num)
	return string.rep("\n",num)
end

function tab(num)
	return string.rep("\t",num)
end


-- Set/Get class name

function header_file.set_class(name)
	class = name
end

function header_file.is_grid_instance(is_gi)
	is_grid_instance = is_gi
end

function header_file.get_class()
	return class
end

-- Define guards
function get_def_prefix(upper_name)
	tmp = ""
	for token in string.gmatch(upper_name, "[^%s]+") do
	   tmp = tmp .. "_" .. token
	end
	return "_AR"..tmp .."_H"
end

function get_ifdef(upper_name)
	return "#ifndef " .. get_def_prefix(class_upper) .. newline(1)
end

function get_def(upper_name)
	return "#define " .. get_def_prefix(class_upper) .. newline(2)
end

function get_def_guard_start()
	class_upper = string.upper(class)
	return  get_ifdef(class_upper) .. get_def(class_upper_name)
end

function get_def_guard_end()
	return "#endif"
end

-- Includes
function get_includes()
	incl = "#include \"ar_instance_base.h\n"
	if is_grid_instance then
		incl = incl .. "#include \"grid_instance_base.h\n"
	end

	return incl

end

-- Class definition
function get_class_name(name)
	tmp = ""
	for token in string.gmatch(name, "[^%s]+") do
	   tmp = tmp .. (token:gsub("^%l", string.upper))
	end
	return "AR" .. tmp
end

function get_class_definition()
	res = "class " .. get_class_name(class) .. " : public ARInstance "
	if is_grid_instance then
		res = res .. ", public GridInstanceBase"
	end
	res = res .. "\n{\n"
	return res
end


function get_end_definition()
	return "};" .. newline(2)
end

-- Class body
function get_class_body(usage)
	res =  tab(1) .. "void initAR(cl::Context context);" .. newline(2);

	if usage == 0 or usage == 2 then
		res = res .. tab(1) .. "void initCL(cl::Context context);" .. newline(1);
		res = res .. tab(1) .. "void runCL();" .. newline(2);
	end

	if usage == 0 or usage == 2 then
		res = res .. tab(1) .. "void initGL();" .. newline(1)
		res = res .. tab(1) .. "void drawGL(UserCamera* uc);" .. newline(2)
	end
	return "public:" .. newline(2) .. tab(1) .. get_class_name(class) .. "();" .. newline(1) .. res
end


-- Generate header file
function get_header_file()
	out_src = get_def_guard_start() .. get_class_definition() .. get_class_body(2) .. get_end_definition() .. get_def_guard_end()
	return out_src
end

function header_file.gen_file()
	print(get_header_file())
	class_lower = string.lower(class)
	file_name = ""
	for token in string.gmatch(class_lower, "[^%s]+") do
		if file_name == "" then
			file_name = token
		else
			file_name = file_name .. "_" .. token
		end
	end
	file_name = file_name .. ".h"

	file = io.open(file_name,"w+")
	io.output(file)
	io.write(get_header_file())

	io.close()
end




return header_file
