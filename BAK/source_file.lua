local source_file = {
 class = "",
 usage = nil,
 is_grid_instance = false
}

-- Misc

function newline(num)
	return string.rep("\n",num)
end

function tab(num)
	return string.rep("\t",num)
end

function bracket()
	return "{" .. newline(2) .. "}" .. newline(1)
end


function get_file_name(ext)
	class_lower = string.lower(class)
	file_name = ""

	for token in string.gmatch(class_lower, "[^%s]+") do
		if file_name == "" then
			file_name = token
		else
			file_name = file_name .. "_" .. token
		end
	end
	return file_name .. "." .. ext

end


-- Set/Get

function source_file.set_class(name)
	class = name
end

function source_file.is_grid_instance(is_gi)
	is_grid_instance = is_gi
end

function source_file.get_class()
	return class
end

function source_file.set_usage(u)
	usage = u
end

function source_file.is_grid_instance(is_gi)
	is_grid_instance = is_gi
end

-- Define guards
function get_def_prefix(upper_name)
	tmp = ""
	for token in string.gmatch(upper_name, "[^%s]+") do
	   tmp = tmp .. "_" .. token
	end
	return "_AR"..tmp .."_H"
end

function get_ifdef(upper_name)
	return "#ifndef " .. get_def_prefix(class_upper) .. newline(1)
end

function get_def(upper_name)
	return "#define " .. get_def_prefix(class_upper) .. newline(2)
end

function get_def_guard_start()
	class_upper = string.upper(class)
	return  get_ifdef(class_upper) .. get_def(class_upper_name)
end

function get_def_guard_end()
	return "#endif"
end



-- Includes cpp
function get_includes()
	return "#include \"" .. get_file_name("h") .. "\"" .. newline(2)
end


-- Includes h
function get_header_includes()
	incl = "#include \"ar_instance_base.h\n"
	if is_grid_instance then
		incl = incl .. "#include \"grid_instance_base.h\n"
	end

	return incl

end


-- Class definition
function get_class_name(name)
	tmp = ""
	for token in string.gmatch(name, "[^%s]+") do
	   tmp = tmp .. (token:gsub("^%l", string.upper))
	end
	return "AR" .. tmp
end


function get_class_definition()
	res = "class " .. get_class_name(class) .. " : public ARInstance "
	if is_grid_instance then
		res = res .. ", public GridInstanceBase"
	end
	res = res .. "\n{\n"
	return res
end

function get_end_definition()
	return "};" .. newline(2)
end

-- Class definition body
function get_class_body()
	res =  tab(1) .. "void initAR(cl::Context context);" .. newline(2);

	if usage == 0 or usage == 2 then
		res = res .. tab(1) .. "void initCL(cl::Context context);" .. newline(1);
		res = res .. tab(1) .. "void runCL();" .. newline(2);
	end

	if usage == 1 or usage == 2 then
		res = res .. tab(1) .. "void initGL();" .. newline(1)
		res = res .. tab(1) .. "void drawGL(UserCamera* uc);" .. newline(2)
	end
	return "public:" .. newline(2) .. tab(1) .. get_class_name(class) .. "();" .. newline(1) .. res
end

-- Function bodies

function get_body_prefix(str)
	return str .. "void " ..  get_class_name(class)
end

function get_function_bodies()
	res =  get_body_prefix(" ") .. "::initAR(cl::Context context)" .. bracket()

	if usage == 0 or usage == 2 then
		res = get_body_prefix(res) .. "::initCL(cl::Context context)" .. bracket()
		res = get_body_prefix(res) .. "::runCL()" .. bracket()
	end

	if usage == 1 or usage == 2 then
		res = get_body_prefix(res) .. "::initGL()" 				 .. bracket()
		res = get_body_prefix(res) .. "::drawGL(UserCamera* uc)"   .. bracket()
	end
	return get_class_name(class) .. "::" ..  get_class_name(class) .. "()" .. bracket() .. res
end


-- Generate header content
function get_header_content()
	return get_def_guard_start() .. get_class_definition() .. get_class_body() .. get_end_definition() .. get_def_guard_end()
end

-- Generate cpp content
function gen_cpp_conent()
	return get_includes()  .. get_function_bodies()
end


-- IO

function create_file(content,file_name)
	file = io.open(file_name,"w+")
	io.output(file)
	io.write(content)
	io.close()

end

function gen_header_file()
	create_file(get_header_content(),get_file_name("h"))
end

function gen_cpp_file()
	create_file(gen_cpp_conent(),get_file_name("cpp"))
end

function source_file.gen_files()
	gen_header_file()
	gen_cpp_file()
	print(get_file_name("h") .. " and " .. get_file_name("cpp") .. " created.")
end



return source_file
