package.path = 'SLAXML/?.lua;' .. package.path

local SLAXML = require 'slaxdom'
local xml  = io.open('source_template.xml'):read('*all')
local doc = SLAXML:dom(xml)
local lvl = -1


function tab(num)
	return string.rep("\t",num)
end

function trim(s)
	P = string.gsub(s, "%s", "")
	return P
end


function print_node(node)
	lvl = lvl + 1
	if (type(node) == "table") then
		for k,v in pairs(node) do
			print(tab(lvl), k)
			if k~="parent" then
				print_node(v)
			end
		end
	elseif (type(node) == "number" or type(node) == "string") then
		print(tab(lvl),node)
	end
	lvl = lvl - 1
end

function get_node(node,node_name,nodes)

	if node == node_name then
		return 0
	end

	if (type(node) == "table") then

		for k,v in pairs(node) do

			if k == node_name then
				return 1
			end

			if k~="parent" then
				flag = get_node(v,node_name,nodes)
				if flag == 0  then
					if node.attr[1] == nil then
						nodes[node.name] = node.kids
					else
						nodes[node.attr[1].value] = node.kids
					end
					return 2
				end
			end

		end
	end
	return 2

end






local tmp_lvl = 0
function get_value2(node,tbl,max_lvl)

	if tmp_lvl == max_lvl then
		tmp_lvl = tmp_lvl - 1
		return
	end



	if (type(node) == "table") then
		for k,v in pairs(node) do
			if k~="parent"  then

				print(tab(tmp_lvl),k,v,tmp_lvl)
				tmp_lvl = tmp_lvl + 1

				if v.attr ~= nil and v.attr.id then
					if tbl[v.name] == nil then
						tbl[v.name] = {}
					end
					tbl[v.name][v.attr.id] = {}
					get_value2(v,tbl[v.name][v.attr.id],max_lvl)
				elseif v.kids ~= nil and v.kids[1].type =="text" and trim(v.kids[1].value) ~="" then
					tmp_lvl = tmp_lvl - 1
					tbl[v.name] = v.kids[1].value
				elseif v.type == "element" then
					tbl[v.name] = {}
					get_value2(v,tbl[v.name],max_lvl)
				else
					get_value2(v,tbl,max_lvl)
				end

			end
		end
	end

	tmp_lvl = tmp_lvl - 1
end


nodes = {}

get_node(doc.root,"catalog",nodes)

tbl = {}
get_value2(doc.root,tbl,20)

print_node(tbl)

